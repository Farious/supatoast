﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Movement))]
public class CatchObjects : MonoBehaviour {
    private Movement toast;

	// Use this for initialization
	void Start () {
        toast = GetComponent<Movement>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Catchables"))
        {
            float points = collider.gameObject.GetComponent<CatchableObject>().points;
            toast.score += points;
            GameObject.Destroy(collider.gameObject);
        }
    }
}
