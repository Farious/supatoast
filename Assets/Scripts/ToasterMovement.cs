﻿using UnityEngine;
using System.Collections;

public class ToasterMovement : MonoBehaviour {                                 
    public float minPos = 13f;
    public float maxPos = 21f;
    public float velocity = 0.5f;
    public float velRate = 0.01f;

    private GameObject toast;
    private Movement toastScript;

    public float force = 0f;

    private int state = 0;

    /// <summary>
    /// 
    /// </summary>
    void Start()
    {
        toast = GameObject.FindGameObjectWithTag("Player");
        toastScript = toast.GetComponent<Movement>();
    }

	/// <summary>
	/// 
	/// </summary>
	void Update () {
        if (state == -1)
        {
            return;
        }

        if (DoCalcForce())
        {
            Vector3 pos = transform.localPosition;
            if (state == 2)
            {
                PushToast();
                state = -1;
                pos.y = maxPos;
            }
            else
            {
                velocity *= (pos.y >= maxPos || pos.y <= minPos) ? -1 : 1;

                pos.y += velocity;

                pos.y = (pos.y > maxPos) ? maxPos : pos.y;
                pos.y = (pos.y < minPos) ? minPos : pos.y;
            }

            transform.localPosition = pos;
        }
	}

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    bool DoCalcForce()
    {
        float movement = 1f;
        movement *= Input.GetMouseButton(0) ? 1.0f : 0.0f;

        switch (state)
        {
            case 0:
                if (movement == 0f)
                {
                    return false;
                }
                else
                {
                    state = 1;
                }
                break;
            case 1:
                if (movement == 1f)
                {
                    break;
                }
                else
                {
                    state = 2;
                }
                break;
            case 2:
                return true;
            default:
                return false;
        }

        // If we are here it's because we should calculate the output force.
        force += movement;
        velocity *= 1f + velRate;
        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    void PushToast()
    {
        toastScript.ConstantForce( force );
        force = 0f;
        velocity = 0.5f;
    }
}
