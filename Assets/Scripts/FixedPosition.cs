﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FixedPosition : MonoBehaviour {
    private GameObject toast;
    private float toastWidth;
    private float floatable = 1f;
    public float deltaToToast = 0f;
    private bool start = false;

    public float delta = 2f;
    public float decorrido = 0f;
    public List<string> catchables;

    public float deltaToCatchable = 0f;
    public float nextCatchable = 2f;
    void Awake ()
    {
        GameObject[] objects = Resources.LoadAll<GameObject>("Catchables");
        foreach (var item in objects)
        {
            if (item.name.EndsWith("_object"))
            {
                catchables.Add(item.name);
            }
        }
    }

	// Use this for initialization
	void Start () {
        toast = GameObject.FindGameObjectWithTag("Player");
        toastWidth = toast.GetComponent<SpriteRenderer>().sprite.rect.width / 2f;
	}
	
	// Update is called once per frame
	void Update () {        
        Vector3 position = toast.transform.position;

        if ((position.y - transform.position.y) > Camera.main.orthographicSize && !start)
        {
            decorrido += Time.deltaTime;

            if (decorrido > delta)
            {
                start = true;
            }
        }

        if (start)
        {
            position.z = transform.position.z;
            position.y -= deltaToToast;
            position.x = 0f;

            transform.position = position;

            if (transform.position.y - (2 * Camera.main.orthographicSize) > -7f)
            {
                RandomObjectSpawn();
            }
        }

        
	}

    void ForceGameplayArea (float _width = -1f)
    {
        float width = _width;
        if (_width == -1f)
        {
            width = Screen.width * Camera.main.rect.width / floatable / 2.0f - toastWidth;
        }
        toast.GetComponent<Movement>().playRadious = width;
    }

    void RandomObjectSpawn ()
    {
        float aspect = Camera.main.aspect;
        float size = Camera.main.orthographicSize * aspect;

        if (deltaToCatchable > nextCatchable)
        {
            int val = Random.Range(0, catchables.Count);
            GameObject instance = (GameObject)
                Instantiate((Object)Resources.Load<GameObject>(
                    "Catchables/" + catchables[val]
                ));

            Vector3 delta = new Vector3(
                Random.Range(-0.5f * size, size * 0.5f),
                transform.position.y - (2 * Camera.main.orthographicSize),
                0f);

            instance.transform.position = delta;
            deltaToCatchable = 0f;
            nextCatchable = Random.Range(1f, 3f);
        }
        else
        {
            deltaToCatchable += Time.deltaTime;
        }
        
    }
}
