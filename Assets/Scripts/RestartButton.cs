﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(dfButton))]
public class RestartButton : MonoBehaviour {
	// Update is called once per frame
	void OnClick () {
        Application.LoadLevel(Application.loadedLevel);
	}
}
