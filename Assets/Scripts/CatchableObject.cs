﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class CatchableObject : MonoBehaviour {
    private SpriteRenderer catchable;
    
    public float points = 100f;

    void Awake ()
    {
        transform.Rotate(Vector3.forward, Random.Range(0, 360));
        transform.localScale = Vector3.one * Random.Range(0.7f, 1.5f);
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        float gameArea = (Camera.main.orthographicSize * 2) + Camera.main.transform.position.y;
        if (transform.position.y > gameArea)
        {
            GameObject.Destroy(gameObject);
        }
	}
}
