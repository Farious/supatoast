﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
    public float playRadious = 85f;
    public float forceMovement = 1f;
    public float decay = 1f;
    public float force = 0f;
    public float maxHeight = 1f;
    public float height = 0f;
    public float score = 0f;
    public HeightMark heightMark;

    private bool mousePrevState = false;
    private Vector3 mousePrevPos = Vector3.zero;
    private float screenWidth = 0f;
    private float gameWidth = 480f;
    private enum toastState { Stopped, Up, Down };
    private toastState state = toastState.Stopped;
    private Rect spriteRect;

    void Awake()
    {
        spriteRect = GetComponent<SpriteRenderer>().sprite.rect;
        playRadious = 100f - spriteRect.width / 2f;
    }

	// Use this for initialization
	void Start () {
        heightMark = GameObject.FindGameObjectWithTag("HeightMarker")
                                .GetComponent<HeightMark>();
        screenWidth = Screen.width;
        forceMovement = gameWidth / screenWidth;
	}

    public void ConstantForce(float m_force)
    {
        force = m_force;

        if (force > 0f)
        {
            state = toastState.Up;
        }
        else if (force < 0f)
        {
            state = toastState.Down;
        }
        else
        {
            state = toastState.Stopped;
        }
    }

    void AddForce()
    {
        transform.position += Vector3.up * force * decay;

        if (decay < 0.0001f)
        {
            decay = 1f;

            switch (state)
            {
                case toastState.Stopped:
                    state = toastState.Stopped;
                    break;
                case toastState.Up:
                    state = toastState.Down;
                    break;
                case toastState.Down:
                    state = toastState.Stopped;
                    break;
                default:
                    state = toastState.Stopped;
                    break;
            }
        }
    }

    float processMouseInput()
    {
        bool mouseState = Input.GetMouseButton(0);
        float deltaPosition = 0f;

        if (mouseState)
        {
            if (!mousePrevState)
            {
                mousePrevPos = Input.mousePosition;
            }

            deltaPosition = Input.mousePosition.x - mousePrevPos.x;
            mousePrevPos = Input.mousePosition;
        }
        else
        {
            mousePrevPos = Vector3.zero;
        }

        mousePrevState = mouseState;

        return deltaPosition;
    }

    void GoingUp()
    {
        decay *= (1f - Time.fixedDeltaTime);
        AddForce();
        CalcMaxHeight();
    }

    void CalcMaxHeight()
    {
        if (height >= maxHeight)
        {
            score += height - maxHeight;
            maxHeight = height;
        }
        else
        {
            state = toastState.Down;
            decay = 1f;
            if (force > 0f)
            {
                force = 0f;
            }
        }

        heightMark.height = height / maxHeight;
    }

    void GoingDown()
    {
        decay = 1f;
        if (force > -5f) force -= 0.01f;

        if (height < -7 + (spriteRect.height/2f))
        {
            height = -7;
            force = 0f;
            state = toastState.Stopped;
        }

        heightMark.height = height / maxHeight;
        AddForce();
    }

    Vector3 ProcessHorizontalMovement(float horizontalMovement)
    {
        Vector3 pos = transform.position;

        pos += new Vector3(horizontalMovement, 0f, 0f);

        if (pos.x > playRadious)
        {
            pos = new Vector3(playRadious, pos.y, pos.z);
        }

        if (pos.x < -1 * playRadious)
        {
            pos = new Vector3(-1 * playRadious, pos.y, pos.z);
        }

        return pos;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case toastState.Stopped:
                return;
            case toastState.Up:
                GoingUp();
                break;
            case toastState.Down:
                GoingDown();
                break;
            default:
                return;
        }

        float horizontalMovement = forceMovement;
        float delta = processMouseInput();
        horizontalMovement = delta * forceMovement;

        transform.position = ProcessHorizontalMovement(horizontalMovement);

        height = transform.position.y;
    }
}
